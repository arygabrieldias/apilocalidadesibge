package com.evoluum.arydias.ibge.apiLocalidades.resources;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.evoluum.arydias.ibge.apiLocalidades.models.Municipio;
import com.evoluum.arydias.ibge.apiLocalidades.models.Uf;
import com.evoluum.arydias.ibge.apiLocalidades.utils.CsvExportData;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST API Localidades IBGE")
@RestController
@RequestMapping("/download")
public class CsvDownloadResource {

	@Autowired
	MunicipioResource municipioResource;
	
	@Autowired
	UfResource ufResource;
	
	@ApiOperation(value="Download CSV")
	@GetMapping
	public ResponseEntity<Object> downloadCsvFile() throws IOException {
		
		FileWriter fileWriter = null;
		
		List<Uf> listaEstados = null;
		List<CsvExportData> csvDataList = null;
		 
		try {
			
			csvDataList = new ArrayList<>();
			
			listaEstados = ufResource.getListaUf();
			
			//System.out.println(listaEstados.get(1).getId());
			
			
			for(Uf estados : listaEstados) {
				
				List<Municipio> listMun = 
						municipioResource.getMunicipioByUf(String.valueOf(estados.getId()));
				
				for(Municipio mun : listMun) {
					CsvExportData csvDataExport = new CsvExportData();
					
					csvDataExport.setIdEstado(String.valueOf(estados.getId()));
					csvDataExport.setSiglaEstado(estados.getSigla());
					csvDataExport.setRegiaoNome(estados.getRegiao().getNome());
					csvDataExport.setNomeCidade(mun.getNome());
					csvDataExport.setNomeMesorregiao(mun.getMicrorregiao().getMesorregiao().getNome());
					csvDataExport.setNomeFormatado(mun.getNome() + "/" + estados.getSigla());
					
					csvDataList.add(csvDataExport);
				}
			}
			
			StringBuilder fileContent = 
					new StringBuilder("idEstado, siglaEstado, regiaoNome, nomeCidade, nomeMesorregiao, nomeFormatado\n");
			
			for(CsvExportData list : csvDataList) {
				fileContent.append(list.getIdEstado())
				           .append(",")
				           .append(list.getSiglaEstado())
				           .append(",")
				           .append(list.getRegiaoNome())
				           .append(",")
				           .append(list.getNomeCidade())
				           .append(",")
				           .append(list.getNomeMesorregiao())
				           .append(",")
				           .append(list.getNomeFormatado())
				           .append("\n");
			}
			
			String fileName ="estadosMunicipiosMesorregiao.csv";
			fileWriter = new FileWriter(fileName);
			fileWriter.write(fileContent.toString());
			fileWriter.flush();
			
			File file = new File(fileName);
			
			InputStreamResource resource = new InputStreamResource(new FileInputStream(file));
			HttpHeaders headers = new HttpHeaders();
			headers.add("Content-Disposition", String.format("attachment: filename=\"%s\"", file.getName()));
			headers.add("Cache-Control", "no-cache, no-store, must-revalidate");
			headers.add("Pragma", "no-cache");
			headers.add("Expires", "0");
			
			ResponseEntity<Object> responseEntity =
					ResponseEntity.ok().headers(headers).contentLength(file.length()).contentType(MediaType.parseMediaType("application/txt")).body(resource);
			
			return responseEntity;
							
		} catch (Exception e) {
			return new ResponseEntity<>("error occurred", HttpStatus.INTERNAL_SERVER_ERROR);
		} finally {
			
		}
		
	}
	
}
