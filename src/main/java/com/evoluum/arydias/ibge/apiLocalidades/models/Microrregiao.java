package com.evoluum.arydias.ibge.apiLocalidades.models;

public class Microrregiao {
	
	private long id;
	private String nome;
	private Mesorregiao mesorregiao;
	
	public Microrregiao() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Microrregiao(long id, String nome, Mesorregiao mesorregiao) {
		super();
		this.id = id;
		this.nome = nome;
		this.mesorregiao = mesorregiao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Mesorregiao getMesorregiao() {
		return mesorregiao;
	}

	public void setMesorregiao(Mesorregiao mesorregiao) {
		this.mesorregiao = mesorregiao;
	}

}
