package com.evoluum.arydias.ibge.apiLocalidades.models;

public class Regiao {
	
	private long id;
	private String nome;
	private String sigla;
	
	public Regiao() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Regiao(long id, String nome, String sigla) {
		super();
		this.id = id;
		this.nome = nome;
		this.sigla = sigla;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
}
