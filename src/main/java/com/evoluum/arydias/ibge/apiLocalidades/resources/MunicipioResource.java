package com.evoluum.arydias.ibge.apiLocalidades.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.evoluum.arydias.ibge.apiLocalidades.models.Municipio;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST API Localidades IBGE")
@RestController
@RequestMapping("/municipio")
public class MunicipioResource {
	
	@Autowired
	private RestTemplate restTemplate;
	
	public MunicipioResource() {
		super();
		// TODO Auto-generated constructor stub
	}

	@ApiOperation(value="Listar Municípios por UF.")
	@GetMapping(path = "/{idEstado}")
	public @ResponseBody List<Municipio> getMunicipioByUf(@PathVariable("idEstado") String idEstado) {
		
		List<Municipio> municipios = null;
		
		try {
			
			String url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados/" + idEstado + "/municipios";
			
			restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			
			ResponseEntity<List<Municipio>> response = 
					restTemplate.exchange(url, 
					                      HttpMethod.GET, 
					                      null, 
					                      new ParameterizedTypeReference<List<Municipio>>(){});
			
			municipios = response.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return municipios;
				
	}
	
	@ApiOperation(value="Buscar cidade por Nome.")
	@GetMapping(value = "/{nomeCidade}", 
			    produces = MediaType.TEXT_PLAIN_VALUE)
	public String getMunicipioByNome(@PathVariable(value="nomeCidade") String nomeCidade) {
		
		String id = "Not Work";
		
		try {
			
			List<Municipio> listaMunicipios = getMunicipios();
			
			if(listaMunicipios != null) {
				
				for(int i = 0; i < listaMunicipios.size(); i++) {
					
					if(listaMunicipios.get(i).getNome().equalsIgnoreCase(nomeCidade)) {
						id = String.valueOf(listaMunicipios.get(i).getId());
						break;
					}
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return id;
		
	}
	
	@ApiOperation(value="Listar Municípios.")
	@GetMapping
	public @ResponseBody List<Municipio> getMunicipios() {
		
		List<Municipio> municipios = null;
		
		try {
			
			String url = "https://servicodados.ibge.gov.br/api/v1/localidades/municipios";
			
			restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
			
			ResponseEntity<List<Municipio>> response = 
					restTemplate.exchange(url, 
					                      HttpMethod.GET, 
					                      null, 
					                      new ParameterizedTypeReference<List<Municipio>>(){});
			
			municipios = response.getBody();
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return municipios;
		
	}
}
