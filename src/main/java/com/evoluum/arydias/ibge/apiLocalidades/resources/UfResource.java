package com.evoluum.arydias.ibge.apiLocalidades.resources;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.evoluum.arydias.ibge.apiLocalidades.models.Uf;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(value="API REST API Localidades IBGE")
@RestController
@RequestMapping("/uf")
public class UfResource {
	
	@Autowired
	private RestTemplate restTemplate;
	
	@ApiOperation(value="Listar Estados")
	@GetMapping
	public @ResponseBody List<Uf> getListaUf() {
		
		String url = "https://servicodados.ibge.gov.br/api/v1/localidades/estados";
		
		restTemplate.setRequestFactory(new HttpComponentsClientHttpRequestFactory());
		
		ResponseEntity<List<Uf>> response = 
				restTemplate.exchange(url, 
				                      HttpMethod.GET, 
				                      null, 
				                      new ParameterizedTypeReference<List<Uf>>(){});
		
		List<Uf> estados = response.getBody();
		
		return estados;
				
	}

}
