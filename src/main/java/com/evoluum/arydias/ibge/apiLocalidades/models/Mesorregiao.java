package com.evoluum.arydias.ibge.apiLocalidades.models;

public class Mesorregiao {
	
	private long id;
	private String nome;
	private Uf uf;
	
	public Mesorregiao() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public Mesorregiao(long id, String nome, Uf uf) {
		super();
		this.id = id;
		this.nome = nome;
		this.uf = uf;
	}
	
	public long getId() {
		return id;
	}
	
	public void setId(long id) {
		this.id = id;
	}
	
	public String getNome() {
		return nome;
	}
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public Uf getUf() {
		return uf;
	}
	
	public void setUf(Uf uf) {
		this.uf = uf;
	}

}
