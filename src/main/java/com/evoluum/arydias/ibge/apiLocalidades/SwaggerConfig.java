package com.evoluum.arydias.ibge.apiLocalidades;

import static springfox.documentation.builders.PathSelectors.regex;

import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
	public Docket productApi() {
		
		return new Docket(DocumentationType.SWAGGER_2).select()
				.apis(RequestHandlerSelectors.basePackage("com.evoluum.arydias.ibge.apiLocalidades"))
				.paths(regex("/uf.*")).paths(regex("/municipio.*"))
				.build()
				.apiInfo(metaInfo());
				
	}
	
	private ApiInfo metaInfo() {
		
		ApiInfo apiInfo = new ApiInfo("API Localidades REST",
				                      "API REST consumindo serviço externo",
				                      "1.0",
				                      "Terms of Service",
				                      new Contact("Ary Gabriel", 
				                    		      "https://www.youtube.com",
				                    		      "arygabrieldias@gmail.com"),
				                      "Apache License Version 2.0",
				                      "https://www.apache.org/license.html", null
				                      );
		
		return apiInfo;
	}

}
