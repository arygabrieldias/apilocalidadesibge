package com.evoluum.arydias.ibge.apiLocalidades.models;

public class Municipio {
	
	private long id;
	private String nome;
	private Microrregiao microrregiao;
	
	public Municipio() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Municipio(long id, String nome, Microrregiao microrregiao) {
		super();
		this.id = id;
		this.nome = nome;
		this.microrregiao = microrregiao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Microrregiao getMicrorregiao() {
		return microrregiao;
	}

	public void setMicrorregiao(Microrregiao microrregiao) {
		this.microrregiao = microrregiao;
	}

}
