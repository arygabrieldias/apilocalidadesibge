package com.evoluum.arydias.ibge.apiLocalidades.models;

public class Uf {
	
	private long id;
	private String nome;
	private String sigla;
	private Regiao regiao;
	
	public Uf() {
	}

	public Uf(long id, String nome, String sigla, Regiao regiao) {
		super();
		this.id = id;
		this.nome = nome;
		this.sigla = sigla;
		this.regiao = regiao;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	public Regiao getRegiao() {
		return regiao;
	}

	public void setRegiao(Regiao regiao) {
		this.regiao = regiao;
	}

}
